import {Component, Input, ViewChild} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Telecom } from '../model/telecomService';
import { TelecomService } from '../service/telecom.service';
import { Site } from '../model/site';
import { LocalisationService } from '../service/localisation.service';
import { APIResponse } from '../model/apiResponseSite';
import { l3VPN } from '../model/l3VPN';
import { forkJoin } from 'rxjs';
import {ThemePalette} from "@angular/material/core";
import {withInMemoryScrolling} from "@angular/router";

declare const L: any;
@Component({
  selector: 'app-telecom',
  templateUrl: './telecom.component.html',
  styleUrls: ['./telecom.component.css'],

})

export class TelecomComponent {
  formvpn!: FormBuilder;
  selectedValue: string = '';
  @ViewChild('modal1') modal1!: TemplateRef<any>;
  @ViewChild('modal2') modal2!: TemplateRef<any>;
  @ViewChild('modal3') modal3!: TemplateRef<any>;
  @ViewChild('modal4') modal4!: TemplateRef<any>;
  @ViewChild('modal5') modal5!: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @Input() sideNavStatus: boolean = false;
  colorTheme: ThemePalette = 'primary';

  constructor(private builder: FormBuilder, private toastr: ToastrService, private serviceAuth: AuthService ,
              private dialog: MatDialog, private modalService: NgbModal, private service: TelecomService, private localisationService: LocalisationService) { }

  editdata: any;
  dataSource: any;
  telecomService: Telecom = new Telecom();
  checkedSiteIds: number[] = [];
  selectedOptions: string[] = [];
  public sites: Site[] = [];
  public vpn: l3VPN[] = [];
  telecomServiceName = 'web filtring';
  appControlServiceName = 'app control';
  selectedService = "" ;
  ipsServiceName = 'Ips';
  selectedBandwidth = "0";
  nbrBandwidth: string[] = ['20', '50', '100', '150'];
  runningServices: any[] = [];
  public id : number | undefined ;
  onSelectionChange(event: any) {
    this.selectedOptions = event.target.selectedOptions;
  }
  // Méthode pour ouvrir la modale
  open(content: any) {
    this.modalService.open(content);
  }
  openWithCheck(content: any) {
    this.service.getServiceInternet().subscribe(response => {
      if (response && response.length > 0) {
        this.open(content);
        this.runningServices = response;


      } else {
        this.toastr.warning("Il n'y a actuellement aucun service internet en cours d'exécution");
      }
    });
  }
  openWithCheckAuth(content: any) {
      if (this.serviceAuth.getrole() == 'Admin') {
        this.open(content);
      } else {
        this.toastr.warning("Vous n'avez pas l'accès à ce service.");
      }

  }
  getBandwidthForSite(siteId: number): number | null {
    for (let service of this.runningServices) {
      if (service.idSite === siteId) {
        return service.bandWidth;
      }
    }
    return null;
  }

  registerform = this.builder.group({

    bandwidth: this.builder.control('', [
      Validators.required,
      Validators.pattern(
        '(?=.*[0-9])'
      ),
    ]),
  });

  get bandwidth() {
    console.log(this.registerform.get('bandWidth'))
    return this.registerform.get('bandWidth');

  }

  updateCheckedSiteIds(siteId: number) {
    if (this.checkedSiteIds.includes(siteId)) {
      this.checkedSiteIds = this.checkedSiteIds.filter(id => id !== siteId);
    } else {
      this.checkedSiteIds.push(siteId);
    }
  }
  updateCheckedSiteIdsInternet(siteId: number) {
    this.checkedSiteIds = [] ;
    this.checkedSiteIds.push(siteId);
    for (let service of this.runningServices) {
      if (service.idSite === siteId) {
        this.selectedBandwidth=service.bandWidth;
        return;
      }
      else {
        this.selectedBandwidth='0';
      }
    }

  }

  bandwidthError = true;

  onBandwidthChange(event: Event) {
    const input = event.target as HTMLInputElement;
    const value = input.value;
    if (!value) {
      this.bandwidthError = true;
      return;
    }
    this.bandwidthError = false;
    this.telecomService.bandwidth = value;
  }


  loading: boolean = false;
  isButtonDisabled: boolean = false;




  confirmVPN() {
    if (this.checkedSiteIds.length === 1 || this.checkedSiteIds.length === 0) {
      this.toastr.error('Veuillez sélectionner au moins deux sites.');
      return;
    }

    this.loading = true; // activer le spinner
    this.isButtonDisabled = true;
    const idCompany = sessionStorage.getItem('idCompany'); // Récupère l'idCompany depuis le sessionStorage
    const idCompanyNumber = idCompany ? parseInt(idCompany, 10) : 0; // Convertir idCompany en nombre ou 0 si null
    this.localisationService.getSitesByIdCompany(idCompanyNumber).subscribe((apiResponse: APIResponse<Site[]>) => {

      if (apiResponse.status === 'Success') {
        const observables = apiResponse.results.map((site) => this.service.getService(site.idSite));
        forkJoin(observables).subscribe((results: Telecom[][]) => {
          const allServices = results.flat(); // Aplatir le tableau imbriqué
          const existingService = allServices.find((service) => service.typeService === 'L3VPN' && service.state === 'running');

          if (existingService) {
            console.log(existingService)
            this.toastr.warning('Vous avez déjà ce service en cours pour ces sites.');
            this.loading = false; // désactiver le spinner
            this.isButtonDisabled = false;
          } else {
            this.telecomService.serviceType = 'L3VPN';
            this.telecomService.siteIds = this.checkedSiteIds;
            this.service.createService(this.telecomService).subscribe((apiResponse: APIResponse<Telecom[]>) => {
              if (apiResponse.status === 'Success' && apiResponse.results) {
                this.toastr.success('Ajouté avec succès');
                this.checkedSiteIds = []

              } else {
                this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
                this.checkedSiteIds = []
              }
            }, (error: any) => {
              console.error(error);
              this.toastr.error("Une erreur s'est produite lors de la création du service. Veuillez réessayer.");
              this.checkedSiteIds = []
            }, () => {
              // Désactiver le spinner une fois que la requête est terminée
              this.loading = false;
              this.checkedSiteIds = []
            });
          }
        });
      }
    });
  }

  confirmNetwork() {
    if (this.checkedSiteIds.length === 0) {
      this.toastr.error('Veuillez sélectionner un site.');
      return;
    }
    if (this.selectedBandwidth === "0") {
      this.toastr.error("Veuillez sélectionner un débit .");
      return;
    }
    this.loading = true; // activer le spinner
    this.isButtonDisabled = true;
    const observables = this.checkedSiteIds.map((siteId) => this.service.getService(siteId));
    forkJoin(observables).subscribe((results: Telecom[][]) => {
      this.checkedSiteIds.forEach((siteId, index) => {
        const bandWidth = this.selectedBandwidth;
        console.log(this.selectedBandwidth)
        console.log(bandWidth)
        const siteServices = results[index];
        const existingService = siteServices.find((service) => service.typeService === 'InternetViaMPLS' && service.state === 'running');
        if (existingService) {
          this.toastr.warning(`Vous avez déjà ce service en cours pour ce site.`);
          this.loading = false;
          this.isButtonDisabled = false;
          this.checkedSiteIds = []
          this.selectedBandwidth = "0";

        } else {
          this.telecomService.serviceType = 'InternetViaMPLS';
          this.telecomService.siteIds = [siteId];
          this.telecomService.bandwidth = bandWidth ;
          console.log(this.telecomService)
          this.service.createService(this.telecomService).subscribe((apiResponse: APIResponse<Telecom[]>) => {
            console.log(apiResponse.results);
            if (apiResponse.status === 'Success' && apiResponse.results) {
              this.toastr.success('Ajouté avec succès');
              this.checkedSiteIds = []
              this.selectedBandwidth = "0";

            } else {
              this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
              this.checkedSiteIds = []
              this.selectedBandwidth = "0";

            }
          }, (error: any) => {
            console.error(error);
            this.toastr.error("Une erreur s'est produite lors de la création du service. Veuillez réessayer.");
            this.checkedSiteIds = []
            this.selectedBandwidth = "0";

          }).add(() => {
            this.loading = false; // désactiver le spinner
            this.checkedSiteIds = []
          });
        }
      });
    });
  }

  confirmFortinet() {
    this.checkedSiteIds.push(this.sites[0].idSite);
    if (!this.selectedService) {
      this.toastr.error('Veuillez sélectionner un service.');
      this.checkedSiteIds=[];
      return;
    }
    this.loading = true; // activer le spinner
    this.isButtonDisabled = true;
    const observables = this.checkedSiteIds.map((siteId) => this.service.getService(siteId));
    forkJoin(observables).subscribe((results: Telecom[][]) => {
      this.checkedSiteIds.forEach((siteId, index) => {
        const Selectedservice = this.selectedService;
        const siteServices = results[index];
        const existingService = siteServices.find((service) => service.serviceName === Selectedservice && service.typeService === 'FortinetServices');
        if (existingService) {
          this.toastr.warning(`Vous avez déjà ce service en cours d'execution.`);
          this.loading = false;
          this.isButtonDisabled = false;
          this.selectedService = "" ;
          this.checkedSiteIds=[];
        } else {
          this.telecomService.serviceType = 'FortinetServices';
          this.telecomService.serviceName = Selectedservice;
          this.telecomService.idUser = sessionStorage.getItem('id') ;
          console.log(this.telecomService)
          this.service.createService(this.telecomService).subscribe((apiResponse: APIResponse<Telecom[]>) => {
            console.log(apiResponse.results);
            if (apiResponse.status === 'Success' && apiResponse.results) {
              this.toastr.success('Ajouté avec succès, vous recevrez un e-mail contenant les informations de connexion de votre compte.');
              this.selectedService = "" ;
              this.checkedSiteIds=[];
            } else {
              this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
              this.selectedService = "" ;
              this.checkedSiteIds=[];

            }
          }, (error: any) => {
            console.error(error);
            this.toastr.error("Une erreur s'est produite lors de la création du service. Veuillez réessayer.");
            this.selectedService = "" ;
            this.checkedSiteIds=[];


          }).add(() => {
            this.loading = false; // désactiver le spinner
            this.selectedService = "" ;
            this.checkedSiteIds=[];

          });

  }
      })
    })
  }

  confirmQos() {
    if (this.selectedBandwidth === "0" && this.checkedSiteIds.length === 0) { // Vérifier si le champ bandwidth est vide
      this.toastr.error("Veuillez s'il vous plait entrez des donnees valides");
      return;
    }
    else if (this.checkedSiteIds.length === 0) { // Vérifier si le champ bandwidth est vide
      this.toastr.error("Veuillez s'il vous selectionnez un site");
      return;
    }
    else if (this.selectedBandwidth === "0") { // Vérifier si le champ bandwidth est vide
      this.toastr.error("Veuillez sélectionner un débit .");
      return;
    }

    this.loading = true; // activer le spinner
    this.isButtonDisabled = true;

    const observables = this.checkedSiteIds.map((siteId) => this.service.getService(siteId));
    forkJoin(observables).subscribe((results: Telecom[][]) => {
      this.checkedSiteIds.forEach((siteId, index) => {
        const bandWidth = this.selectedBandwidth;
        console.log(this.selectedBandwidth)
        console.log(bandWidth)
        const siteServices = results[index];
        const existingService = siteServices.find((service) => service.typeService === 'InternetViaMPLS' && service.state === 'running');
        if (!existingService) {
          this.toastr.warning(`Vous n'avez pas le service internet en cours d'exécution pour ce site`);
          this.loading = false;
          this.isButtonDisabled = false;
          this.checkedSiteIds = []
          this.selectedBandwidth = "0";

        } else {
         if (Number(bandWidth)  === this.getBandwidthForSite(siteId))
          {
            this.toastr.warning("Un débit Internet de " + bandWidth + " mega est déjà en cours d'utilisation. Veuillez sélectionner une autre valeur.");
            this.checkedSiteIds = []
            this.selectedBandwidth = "0";
            this.loading = false;
            this.isButtonDisabled = false;
            return;
          }
          this.telecomService.serviceType = 'QoSInternet';
          this.telecomService.siteIds = [siteId];
          this.telecomService.bandwidth = bandWidth ;
          console.log(this.telecomService)
          this.service.createService(this.telecomService).subscribe((apiResponse: APIResponse<Telecom[]>) => {
            console.log(apiResponse.results);
            if (apiResponse.status === 'Success' && apiResponse.results) {
              this.toastr.success('Ajouté avec succès');
              this.checkedSiteIds = []
            } else {
              this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
              this.checkedSiteIds = []

            }
          }, (error: any) => {
            console.error(error);
            this.toastr.error("Une erreur s'est produite lors de la création du service. Veuillez réessayer.");
            this.checkedSiteIds = []

          }).add(() => {
            this.loading = false; // désactiver le spinner
          });
        }
      });
    });
  }

  ngOnInit() {
    const idCompany = sessionStorage.getItem('idCompany'); // Récupère l'idCompany depuis le sessionStorage
    const idCompanyNumber = idCompany ? parseInt(idCompany, 10) : 0; // Convertir idCompany en nombre ou 0 si null
    this.localisationService.getSitesByIdCompany(idCompanyNumber).subscribe(
      (apiResponse: APIResponse<Site[]>) => {
        if (apiResponse.status === 'Success') {
          this.sites = apiResponse.results;

        }
      },
    );
  }

  onImageClick() {
    console.log('Image cliquée');
    // Code pour gérer le clic sur l'image
  }
}
