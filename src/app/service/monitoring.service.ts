import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Device } from '../model/device';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonitoringService {

  constructor(private http: HttpClient) { }

  GetDeviceById(idDevice:any){
    return this.http.get<{ status : string, results : Device}>('http://localhost:8077/api/v1/Device'+'/'+idDevice);
  }

  public GetDeviceByName(deviceName:String): Observable<any>{
    return this.http.get<{ status : string, results : Device}>('http://localhost:8077/api/v1/Device/getInfoDeviceById?DeviceName'+'='+deviceName)
  }

}
