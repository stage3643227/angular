import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Company } from '../model/company';

@Injectable()
export class ConfigService {
  constructor(private http: HttpClient) { }
}
@Injectable({
  providedIn: 'root'
})
export class CompanyService {

 /* private apiServiceUrl ='http://localhost:8089';*/
 constructor(private http: HttpClient) {  }

 public getCompany(): Observable<any>{
   return this.http.get<Company>('http://localhost:8089/api/v1/Companies/all');
 }

 public addCompany(company: Company): Observable<any>{
   return this.http.post<Company>('http://localhost:8089/api/v1/Companies', company);
 }

 public updateCompany(company: Company): Observable<any>{
   return this.http.put<Company>('http://localhost:8089/api/v1/Companies', company);
 }

 public deleteCompany(idcompany: number): Observable<any>{
   return this.http.delete<void>('http://localhost:8089/api/v1/Companies/${empleeId}');
 }}
