import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Site } from '../model/site';
import { Topology } from '../model/topology';
@Injectable({
  providedIn: 'root'
})
export class LocalisationService {


  constructor(private http: HttpClient) { }
  
  public getSites(): Observable<any>{
    return this.http.get<Site>('http://localhost:8089/api/v1/Sites/all');

    }
  public getSitesByIdCompany(idCompany:number): Observable<any>{
  return this.http.get<Site>('http://localhost:8089/api/v1/Sites/getSitesByIdCompany'+'/'+idCompany);
 
  }
  public findIdTopologyByCompanyId (idTopology:any): Observable<any>{
    return this.http.get<Topology>('http://localhost:8089/api/v1/topologies'+'/'+idTopology);
    
    }

}
