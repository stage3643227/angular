
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Device } from '../model/device';
import { Telecom } from '../model/telecomService';
import { Observable } from 'rxjs/internal/Observable';
import {ServiceResponse} from "../model/ServiceResponse";
import {Site} from "../model/site";
@Injectable({
  providedIn: 'root'
})
export class TelecomService {

  constructor(private http: HttpClient) { }
  public createService(telecomService: Telecom): Observable<any>{
    return this.http.post<Telecom>('http://localhost:8066/api/v1/ServiceTelecom/createService', telecomService)
  }
  public getService(siteId:any): Observable<any>{
    return this.http.get<Telecom>('http://localhost:8066/api/v1/ServiceTelecom/runningServiceBySite' + '/' + siteId)
  }
  public getServiceInternet(): Observable<any>{
    return this.http.get<Telecom>('http://localhost:8066/api/v1/ServiceTelecom/runningServiceInternetQos')
  }
  getServices(type: string, serviceName?: string): Observable<ServiceResponse[]> {
   let url = 'http://localhost:8066/api/v1/ServiceTelecom/getServices?type='+type ;
   if(serviceName){
     url +='&serviceName='+serviceName;
   }
    return this.http.get<ServiceResponse[]>(url);
  }
 public getSiteData(id: number) : Observable<any> {
    return this.http.get('http://localhost:8089/api/v1/Sites/'+id);
  }
  public deleteSiteL3VpnAndL3Vpn(data: any): Observable<any>{
    return this.http.post('http://localhost:8066/api/v1/ServiceTelecom/deleteSiteL3VpnAndL3Vpn', data);
  }
  public deleteServiceInternet(data: any): Observable<any>{
    return this.http.post('http://localhost:8066/api/v1/ServiceTelecom/deleteInternet', data);
  }
  public getInternetResponseBySiteId(id: number) : Observable<any> {
    return this.http.get('http://localhost:8066/api/v1/ServiceTelecom/getInternetResponseBySiteId/'+id);
  }

}
