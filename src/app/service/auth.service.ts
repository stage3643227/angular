import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Role } from '../model/role';
import { Permission } from '../model/permission';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  apiurl = 'http://localhost:8099';
  public getUserById(): Observable<any> {
    return this.http.get<User>('http://localhost:8099/api/v1/Users');
  }
  public postUser(userName: string, password: string): Observable<any> {
    console.log(userName);
    return this.http.post<User>('http://localhost:8099/api/v1/Users/Login', { userName: userName, password: password });
  }
  createNewUser(user: any) {
    return this.http.post<void>('http://localhost:8099/api/v1/Users', user)
  }
  GetUserbyCode(idUser: any) {
    return this.http.get<{ status: string, results: User }>('http://localhost:8099/api/v1/Users' + '/' + idUser);
  }
  public Getall(): Observable<{ status: string, results: User[] }> {
    return this.http.get<{ status: string, results: User[] }>('http://localhost:8099/api/v1/Users/all');
  }
  updateuser(inputdata: any) {
    return this.http.put<Permission>('http://localhost:8099/api/v1/Users', inputdata);
  }
  parametreuser(user:any){
    return this.http.put<User>('http://localhost:8099/api/v1/Users',user);
  }
  deleteUser(idUser: number): Observable<any> {
    return this.http.delete<void>('http://localhost:8099/api/v1/Users' + '/' + idUser);
  }
  public getuserrole(): Observable<{ status: string, results: Role[] }> {
    return this.http.get<{ status: string, results: Role[] }>('http://localhost:8099/api/v1/Roles/all');
  }
  public getuserpermission(): Observable<any> {
    return this.http.get<Permission>('http://localhost:8099/api/v1/Permissions/all')
  }
  isloggedin() {
    return sessionStorage.getItem('email') != null;
  }
  getrole() {
    return sessionStorage.getItem('role') != null ? sessionStorage.getItem('role')?.toString() : '';
  }
  GetAllCustomer(): Observable<any> {
    return this.http.get<User>('http://localhost:8099/api/v1/Users/all');
  }
  Getaccessbyrole(role: any, menu: any) {
    return this.http.get('http://localhost:8099/roleaccess?role=' + role + '&menu=' + menu)
  }
  updateUser(idUser: any) {
    // Send the updated user data to the backend API
    return this.http.put<User>(`http://localhost:8099/api/v1/Users`, idUser)
  }
}
