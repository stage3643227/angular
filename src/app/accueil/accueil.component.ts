import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  navbarToggler: HTMLElement | null = null;

  constructor() { }

  ngOnInit(): void {
    this.initializeScripts();
    this.navbarToggler = document.body.querySelector('.navbar-toggler') as HTMLElement;
  }

  initializeScripts(): void {
    // Navbar shrink function
    const navbarShrink = () => {
      const navbarCollapsible = document.body.querySelector('#mainNav');
      if (!navbarCollapsible) {
        return;
      }
      if (window.scrollY === 0) {
        navbarCollapsible.classList.remove('navbar-shrink');
      } else {
        navbarCollapsible.classList.add('navbar-shrink');
      }
    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
      new bootstrap.ScrollSpy(document.body, {
        target: '#mainNav',
        rootMargin: '0px 0px -40%',
      });
    }

    // Collapse responsive navbar when toggler is visible
    if (this.navbarToggler) {
      const responsiveNavItems = Array.from(
        document.querySelectorAll('#navbarResponsive .nav-link')
      ) as HTMLElement[];
      responsiveNavItems.forEach((responsiveNavItem) => {
        responsiveNavItem.addEventListener('click', () => {
          if (window.getComputedStyle(this.navbarToggler!).display !== 'none') {
            this.navbarToggler!.click();
          }
        });
      });
    }
  }
}
