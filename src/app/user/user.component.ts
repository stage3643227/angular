import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AuthService } from '../service/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { UpdatepopupComponent } from '../updatepopup/updatepopup.component'
import { ToastrService } from 'ngx-toastr';
import { User } from '../model/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements AfterViewInit {
  @Input() sideNavStatus: boolean = false;
  editdata: any;
  constructor(private builder: FormBuilder, private toastr: ToastrService, private service: AuthService, private dialog: MatDialog) {
  }
  userlist: User[] = [];
  dataSource: any;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit() {
    this.LoadUser();
  }
  ngAfterViewInit(): void {
  }
  LoadUser() {
    this.service.Getall().subscribe(res => {
      this.userlist = res.results.filter(item => item.idCompany == Number(sessionStorage.getItem('idCompany')));
      this.dataSource = new MatTableDataSource(this.userlist);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  displayedColumns: string[] = ['Nom', 'Prénom', 'email', 'role', 'action'];

  updateuser(code: any) {
    this.OpenDialog('1000ms', '600ms', code);
  }
  DeleteUser(idUser: number) {
    this.service.deleteUser(idUser).subscribe(res => {
      console.log(this.service.deleteUser(idUser))
      this.editdata = res;
      this.toastr.success("l'Utilisateur est supprimé avec succès.");
      this.userlist = this.userlist.filter(item => item.idUser != idUser);
      this.dataSource = new MatTableDataSource(this.userlist);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  OpenDialog(enteranimation: any, exitanimation: any, code: string) {
    const popup = this.dialog.open(UpdatepopupComponent, {
      enterAnimationDuration: enteranimation,
      exitAnimationDuration: exitanimation,
      width: '30%',
      data: {
        usercode: code
      }
    });
    popup.afterClosed().subscribe(res => {
      this.LoadUser();
    });
  }
}
