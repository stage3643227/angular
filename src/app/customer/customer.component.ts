import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { User } from '../model/user';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})

export class CustomerComponent {
  sideNavStatus: boolean = false;


  constructor(private route: ActivatedRoute, private service: AuthService, private toastr: ToastrService, private router: Router) {
  }
  dataSource: any;
  user: any;
  currentUser: User[] | undefined;
  showPassword = false;

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
    const passwordInput = document.getElementById('password') as HTMLInputElement;
    if (this.showPassword) {
      passwordInput.type = 'text';
    } else {
      passwordInput.type = 'password';
    }
  }
  ngOnInit() {
    this.loaduserdata(sessionStorage.getItem('id'));
    this.loaduserdata(sessionStorage.getItem('idCompany'));
    this.loaduserdata(sessionStorage.getItem('idRole'));
    console.log(sessionStorage.getItem('idCompany'))
    console.log(sessionStorage.getItem('id'))
  this.loaduserdata(sessionStorage.getItem('id')) 

  }
  loaduserdata(code: any) {
    this.service.GetUserbyCode(code).subscribe(
      response => {
        console.log(response.results);
        this.user = response.results;
      },
      error => {
        console.log(error);
      }
    );
  }
  
  
  
  addcustomer() {
    if (this.service.getrole() == '') {
      this.toastr.success('Success')
    } else {
      this.toastr.warning("Error")
    }
  }

  updateUser() {
    const updatedUser = {
      idUser: sessionStorage.getItem('id'),
      idCompany: sessionStorage.getItem('idCompany'),
      idRole: sessionStorage.getItem('idRole'),
      lastName: this.user.lastName,
      firstName: this.user.firstName,
      email: this.user.email,
      password: this.user.password
    };
  
    this.service.updateUser(updatedUser).subscribe(
      response => {
        this.toastr.success('Les informations ont été mises à jour avec succès.');
      },
      error => {
        this.toastr.error('Une erreur est survenue lors de la mise à jour des informations.');
      }
    );
  }
  

}
