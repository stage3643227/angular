export interface Device{
    idDevice:number;
    deviceName:string;
    deviceType:string;
    cpuValue:number;
    memoryValue:number;
    ipAddress:string;
    port:string;
    transport:string;
    idInterface:number;
    nameInterface:string;
    interfaceState:string;
    device: Device[]
}