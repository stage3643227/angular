import {Device} from "./device";

export interface NetInterface{
    idInterface:number;
    nameInterface:string;
    interfaceState:string;
    idDevice:Device;
    device: Device[];
    

}