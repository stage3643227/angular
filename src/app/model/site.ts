export interface Site{
    idSite: any;
    latitude: number;
    longitude: number;
    country:string;
    city:string;
    nameTopology:string;
    nameCompany:string;
    idTopology:number;
  
}