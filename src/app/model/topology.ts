import {Site} from "./site";
export interface Topology{
    idTopology:number;
    nameTopology:string;
    nameCompany:string;
    sites: Site[]; 
}