export interface APIResponse<T> {
    status: string;
    errors: ErrorDTO[];
    results: T;
   }
   interface ErrorDTO {
    code: number;
    message: string;
   }