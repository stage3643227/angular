import { Role } from "./role";
export interface User {
    idUser :number;
    firstName : string;
    lastName : string;
    password: string;
    email: string;
    idCompany : number;
    role: Role[]; 
}