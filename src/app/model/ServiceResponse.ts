import {Telecom} from "./telecomService";
import {Site} from "./site";

export class ServiceResponse {
  service!: Telecom;
  siteDTOS !: Site[];
}
