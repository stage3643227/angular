import { Permission } from "./permission";

export interface Role {
    idRole :number;
    roleName : string;
    permissions: Permission[]
}