import {Device} from "./device";

export interface Vendor{
    idVendor:number;
    vendorName:string;
    driver:string;
    device: Device[]
}