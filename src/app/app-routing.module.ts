import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { AuthGuard } from './guard/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { TopologyComponent } from './topology/topology.component';
import { MapComponent } from './map/map.component';
import { TelecomComponent } from './telecom/telecom.component';
import { AccueilComponent } from './accueil/accueil.component';
import { UserspaceComponent } from './userspace/userspace.component';

const routes: Routes = [
  { component: LoginComponent, path: 'login' },
  { component: RegisterComponent, path: 'register', canActivate: [AuthGuard]},
  { component: HomeComponent, path: 'home', canActivate: [AuthGuard] },
  { component: UserComponent, path: 'user', canActivate: [AuthGuard] },
  { component: CustomerComponent, path: 'customer' },
  { component: TopologyComponent, path: 'topology' },
  { component: MapComponent, path: 'map' },
  { component: TelecomComponent, path: 'telecom' },
  { component: AccueilComponent, path: 'accueil' },
  { path: '', component: AccueilComponent },
 
  { path: 'userspace', component: UserspaceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
