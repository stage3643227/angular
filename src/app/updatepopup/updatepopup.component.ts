import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-updatepopup',
  templateUrl: './updatepopup.component.html',
  styleUrls: ['./updatepopup.component.css']
})
export class UpdatepopupComponent implements OnInit {

  constructor(private builder: FormBuilder, private service: AuthService, private toastr: ToastrService,
    private dialogref: MatDialogRef<UpdatepopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {



  }
  ngOnInit(): void {
    this.service.getuserrole().subscribe(res => {
      console.log(res);
      this.rolelist = res.results.filter(item => item.roleName != 'Admin');
    });

    if (this.data.usercode != '' && this.data.usercode != null) {
      this.loaduserdata(this.data.usercode);
    }
  }
  rolelist: any;
  editdata: any;

  registerform = this.builder.group({
    idUser: this.builder.control(''),
    firstName: this.builder.control(''),
    lastName: this.builder.control(''),
    password: this.builder.control(''),
    email: this.builder.control(''),
    idCompany: this.builder.control(''),
    idRole: this.builder.control(''),
  });

  loaduserdata(code: any) {
    this.service.GetUserbyCode(code).subscribe(res => {
      this.editdata = res.results;
      console.log(this.editdata);
      this.registerform.setValue({
        idUser: this.editdata.idUser,
        firstName: this.editdata.firstName,
        lastName: this.editdata.lastName,
        password: this.editdata.password,
        email: this.editdata.email,
        idRole: this.editdata.role.idRole,
        idCompany: this.editdata.idCompany,
      });
    });
  }


  UpdateUser() {
    this.service.updateuser(this.registerform.value).subscribe(res => {
      console.log(this.service.updateuser(this.registerform.value))
      this.toastr.success('Updated successfully.');
      this.dialogref.close();
    });
  }

}
