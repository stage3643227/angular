import { Component, OnInit, ViewChild } from '@angular/core';
import { Site } from '../model/site';
import { LocalisationService } from '../service/localisation.service';
import { APIResponse } from '../model/apiResponseUser';
import { Company } from '../model/company';
import { MatSidenav } from '@angular/material/sidenav';
import { TelecomService } from '../service/telecom.service';
import { Telecom } from '../model/telecomService';
import {ServiceResponse} from "../model/ServiceResponse";
import {Observable} from "rxjs/internal/Observable";
import {ToastrService} from "ngx-toastr";
import {InternetResponse} from "../model/InternetResponse";

declare const L: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  siteData : any;
  deleting: { [key: number]: boolean; } = {};
  sideNavStatus: boolean = false;
  selectedTab: string = 'l3vpn';
  selectedId !: number ;
  reponse : ServiceResponse[] = [] ;
  InternetResponse = new InternetResponse();
  mymap: any;
  telecomService: Telecom = new Telecom();
  public company: Company[] | undefined;
  public sites: Site[] = [];
  isButtonDisabled: boolean = false;
  services = [
    {service: 'Update software', site: 'Site A', etat: 55, action: 'Update'},
    {service: 'Clean database', site: 'Site B', etat: 70, action: 'Clean'},
  ];
  constructor(private localisationService: LocalisationService, private service: TelecomService,private toastr: ToastrService) { }
 

  ngOnInit() {

    if (!navigator.geolocation) {
      console.log('location is not supported');
    }
    this.mymap = L.map('map',{
      zoom: 1
    });
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = position.coords;
      const latLong = [coords.latitude, coords.longitude];
      console.log(
        `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
      );

      this.mymap.locate({ setView: true, maxZoom: 6 });
      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 20,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.mymap);
    });


    var orangeIcon = L.icon({
      iconUrl: 'assets/marker.png',
      iconSize: [20, 35], // size of the icon
    });

    let marker = []
    let services: Telecom[] = [];
    const idCompany = sessionStorage.getItem('idCompany');
const idCompanyNumber = idCompany ? parseInt(idCompany, 10) : 0;
let serviceActifTrouve = false; // Variable pour suivre si un service actif est trouvé
this.localisationService.getSitesByIdCompany(idCompanyNumber).subscribe((apiResponse: APIResponse<Site[]>) => {
  if (apiResponse.status === 'Success') {
    apiResponse.results.forEach((site) => {
      this.service.getService(site.idSite).subscribe((result) => {
        let services: Telecom[] = result;
        let latlng = [site.latitude, site.longitude];
        let popupText = "<div style='color: black; font-size: 16px; font-family: Arial; font-weight: bold; text-align: center; padding: 10px; border-radius: 5px'>" + site.nameCompany + " " + site.city + "</div>";
        popupText += "<div style='margin-top: 20px; text-align: center'><img src='" + ('assets/services.png') + "' style='max-width: 120px'></div>";
        let servicesActifs = "";

        services.forEach((service) => {
          if (service.typeService === 'L3VPN' && service.state === 'running') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i> L3VPN:  <span style='color: #808080;'> Actif </span></div>";
          } else if (service.typeService === 'InternetViaMPLS' && service.state === 'running') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i> Internet : <span style='color: #808080;'> Actif </span></div>";
          } else if (service.typeService === 'QoSInternet') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i>  Débit internet : <span style='color: #808080;'>"+service.bandWidth+" </span> Méga </div>";
          } else if (service.serviceName === 'webfiltring') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i>  web filtring : <span style='color: #808080;'>  Actif </span> </div>";
          } else if (service.serviceName === 'appControl') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i>  app Control : <span style='color: #808080;'>  Actif </span> </div>";
          } else if (service.serviceName === 'ips') {
            servicesActifs += "<div style='font-size: 15px; font-family: Arial; margin-top: 10px; color: black;'> <i class=\"fa fa-check-circle\" aria-hidden=\"true\" style='color: #FF6600;'></i>  ips : <span style='color: #808080;'>  Actif </span> </div>";
          }
        });

        if (servicesActifs !== "") {
          popupText += "<div style='font-size: 16px; margin-top: 20px; font-family: Arial; font-weight: bold; color: #FF6600'> Vos services en cours :</div><br>" + servicesActifs;
        } else {
          popupText += "<div style='font-size: 16px; margin-top: 20px; font-family: Arial; font-weight: bold; color: #FF6600'> Aucun service en cours. </div><br>";
        }

        popupText += "</div>";
        let marker = L.marker(latlng, { icon: orangeIcon }).addTo(this.mymap);
        marker.bindPopup(popupText).openPopup();
      });
    });
  } else {
    console.error('An error occurred:', apiResponse.errors);
  }
});

    const searchControl = L.esri.Geocoding.geosearch().addTo(this.mymap);
    const results = L.layerGroup().addTo(this.mymap);
    var sidePanel = document.getElementById('side-panel');
    var toggleButton = document.getElementById('toggle-button');

    toggleButton?.addEventListener('click', function () {
      sidePanel?.classList.toggle('visible');
    });
  }
  getServices(type: string, serviceName?: string) {
    this.service.getServices(type, serviceName)
      .subscribe((response) => {
        this.reponse = response;
        console.log(this.reponse)
      });
  }

  deleteSiteL3VpnAndL3Vpn(siteDTOS: Site[], serviceId: number) {
    const siteIds = siteDTOS.map(site => site.idSite);
    const data = {
      "siteIds": siteIds,
      "idService": serviceId
    };
    this.deleting[serviceId] = true;
    this.isButtonDisabled = true;
    this.service.deleteSiteL3VpnAndL3Vpn(data).subscribe(response => {
      this.toastr.success('supprimé avec succès');
      this.onL3vpnClick()
      this.deleting[serviceId] = true;
      this.isButtonDisabled = false;
    }, error => {
      this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
      this.deleting[serviceId] = true;
      this.isButtonDisabled = false;
      this.onL3vpnClick()
    });
  }
  deleteServiceInternet(idSite: number, serviceId: number) {
    this.service.getInternetResponseBySiteId(idSite).subscribe(
      (idInternet : InternetResponse) => {
        this.InternetResponse.idServiceInternet= idInternet.idServiceInternet;
        console.log(this.InternetResponse.idServiceInternet);
        const data = {
          "siteIds": [idSite],
          "idService": this.InternetResponse.idServiceInternet
        };
        this.deleting[serviceId] = true;
        this.isButtonDisabled = true;
        this.service.deleteServiceInternet(data).subscribe(response => {
          this.toastr.success('supprimé avec succès');
          this.onInternetClick();
          this.deleting[serviceId] = true;
          this.isButtonDisabled = false;
        }, error => {
          this.toastr.error("Une erreur s'est produite. Veuillez réessayer.");
          this.deleting[serviceId] = true;
          this.isButtonDisabled = false;
          this.onInternetClick();
        });
      },
      error => {
        console.error('Error fetching InternetResponse', error);
      }
    );
  }

  onL3vpnClick() {
    this.reponse=[]
    this.selectedTab = 'l3vpn';
    this.getServices('L3VPN');


    console.log(this.selectedTab)

  }
  onInternetClick() {
    this.selectedTab = 'QoSInternet';
    this.getServices('QoSInternet');
  }
  onWebfiltringClick() {
    this.selectedTab = 'webfiltring';
    this.getServices("FortinetServices",'webfiltring');
  }

  onAppControlClick() {
    this.selectedTab = 'appControl';
    this.getServices("FortinetServices",'appControl');
  }

  onIpsClick() {
    this.selectedTab = 'ips';
    this.getServices("FortinetServices",'ips');
  }
  loadService(code: any) {
    this.service.getService(code).subscribe((result) => {
      for (const service of result) {
        if (service.typeService === 'L3VPN') {
          console.log('L3vpn: ' + service.state);
        } else if (service.typeService === 'InternetViaMPLS') {
          console.log('Internet via MPLS: ' + service.state);
        } else if (service.typeService === 'QoSInternet') {
          console.log('Qos internet: ' + service.bandWidth);
        }
      }
    });
  }

  getSiteDetails(id: number): any {
    this.service.getSiteData(id).subscribe((apiResponse: APIResponse<Site>) => {
      console.log(apiResponse.results.nameCompany + apiResponse.results.city)
      return apiResponse.results.nameCompany + apiResponse.results.city;
    });
  }
  watchPosition() {
    let desLat = 0;
    let desLon = 0;
    let id = navigator.geolocation.watchPosition(
      (position) => {
        console.log(
          `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
        );
        if (position.coords.latitude === desLat) {
          navigator.geolocation.clearWatch(id);
        }
      },
      (err) => {
        console.log(err);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      }
    )
  }
}


