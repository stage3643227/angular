import { Input, TemplateRef, ViewEncapsulation } from '@angular/core';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MonitoringService } from '../service/monitoring.service';
import { APIResponse } from '../model/apiResponseSite';

declare var go: any;
const $ = go.GraphObject.make; // for conciseness in defining templates.


@Component({
  selector: 'app-topology',
  templateUrl: './topology.component.html',
  styleUrls: ['./topology.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TopologyComponent {

  selectedValue: string = '';
  @ViewChild('modal1') modal1!: TemplateRef<any>;
  @ViewChild('modal2') modal2!: TemplateRef<any>;
  @ViewChild('modal3') modal3!: TemplateRef<any>;
  @ViewChild('modal4') modal4!: TemplateRef<any>;
  @ViewChild('modal5') modal5!: TemplateRef<any>;
  @ViewChild('content') content: any; // Reference to the modal content
  @Input() sideNavStatus: boolean = false;
  @ViewChild('myDiagramDiv') div: any;

  public model: go.GraphLinksModel = new go.GraphLinksModel();
  modalRef: NgbModalRef | undefined;
  constructor(private http: HttpClient, public dialog: MatDialog, private modalService: NgbModal, private monitoringService: MonitoringService) { }
  single: any[] = [];// Array to hold the data for the chart
  ramData: any[] = [];
  cpuData: any[] = [];
  colorScheme: any = {
    domain: ['#3D823B', '#BFBFBF']
  };
  view: [number, number] = [350, 250]; // Set the view dimensions for the chart
  

  ngAfterViewInit() {
    // create a make type from go namespace and assign it to MAKE
    const MAKE = go.GraphObject.make;

    // get the div in the HTML file
    const diagramDiv = this.div.nativeElement;

    // instatiate MAKE with Diagram type and the diagramDiv
    const myDiagram =
      MAKE(go.Diagram, diagramDiv,
        {

        });

    // the template we defined earlier
    myDiagram.nodeTemplate =
      MAKE(go.Node,
        // ...
        MAKE(go.Picture,
          new go.Binding("margin"),
          new go.Binding("width"),
          new go.Binding("height"),
          new go.Binding("source")),

        MAKE(go.TextBlock,
          { stroke: "#212121", font: "bold 13px sans-serif" },
          new go.Binding("text")),
        new go.Binding("location", "loc"),

     

        );
     
    // define a Link template that routes orthogonally, with no arrowhead
    myDiagram.linkTemplate =
      MAKE(go.Link,
        { routing: go.Link },
        MAKE(go.Shape,)); // the link shape

    let model = MAKE(go.GraphLinksModel);

  
  
    model.nodeDataArray = [
      { "key": "1", "text": "CE-TUNIS", source: "assets/router.png", loc: new go.Point(175, 218), width: 70, height: 70, margin: 10,"interfaceState": "true" },
      { "key": "4", "text": "R12", source: "assets/simple-switch.png", loc: new go.Point(306, 218), width: 70, height: 70, margin: 10,"interfaceState": "false" },
      { "key": "3", "text": "VPC 19", source: "assets/pc.png", loc: new go.Point(426, 218), width: 70, height: 70, margin: 10 },
      { "key": "5", "text": "VPC20", source: "assets/pc.png", loc: new go.Point(426, 95), width: 70, height: 70, margin: 10 },
      { "key": "6", "text": "Internal Server", source: "assets/internalServer.png", loc: new go.Point(426, 350), width: 70, height: 70, margin: 10 },
      { "key": "11", "text": "backbone", source: "assets/cloud.png", loc: new go.Point(-200, 130), width: 250, height: 250, margin: 10 },
      { "key": "14", "text": "CE-SFAX", source: "assets/router.png", loc: new go.Point(-380, 218), width: 70, height: 70, margin: 10 },
      { "key": "19", "text": "R29", source: "assets/switch.png", loc: new go.Point(-380, 330), width: 70, height: 70, margin: 10 },
      { "key": "20", "text": "VPC", source: "assets/pc.png", loc: new go.Point(-590, 400), width: 70, height: 70, margin: 10 },
      { "key": "21", "text": "VPC", source: "assets/pc.png", loc: new go.Point(-380, 430), width: 70, height: 70, margin: 10 },
      { "key": "23", "text": "User Sfax", source: "assets/winServer.png", loc: new go.Point(-250, 400), width: 70, height: 70, margin: 10 },

    ];

    model.linkDataArray = [
      { "from": "4", "to": "5" },
      { "from": "4", "to": "3" },
      { "from": "4", "to": "1" },
      { "from": "4", "to": "6" },
      { "from": "11", "to": "1" },
      { "from": "10", "to": "1" },
      { "from": "12", "to": "13" },
      { "from": "11", "to": "12" },
      { "from": "11", "to": "10" },
      { "from": "11", "to": "14" },
      { "from": "15", "to": "14" },
      { "from": "16", "to": "15" },
      { "from": "17", "to": "18" },
      { "from": "18", "to": "25" },
      { "from": "18", "to": "23" },
      { "from": "18", "to": "26" },
      { "from": "18", "to": "21" },
      { "from": "18", "to": "22" },
      { "from": "27", "to": "26" },
      { "from": "14", "to": "19" },
      { "from": "19", "to": "20" },
      { "from": "19", "to": "21" },
      { "from": "19", "to": "23" }
    ];

    myDiagram.model = model;

    myDiagram.addDiagramListener('ObjectSingleClicked', (e: go.DiagramEvent) => {
      const node = e.subject.part;
      if (!(node instanceof go.Node)) return;

      // Check if clicked node has key equal to "1"
      if (node.data.text === 'CE-SFAX') {
        // Call the API and get the data
        this.getDataFromAPICESFAX()
          .then((data: any) => {
            // Open the modal and pass the data to it
            this.openModal(this.modal1, data);
          })
          .catch((error: any) => {
            console.error(error);
          });
      }
      if (node.data.text === 'CE-TUNIS') {
        // Call the API and get the data
        this.getDataFromAPICETUNIS()
          .then((data: any) => {
            // Open the modal and pass the data to it
            this.openModal(this.modal2, data);

          })
          .catch((error: any) => {
            console.error(error);
          });
      }
      if (node.data.text === 'R12') {
        // Call the API and get the data
        this.getDataFromAPISWTUNIS()
          .then((data: any) => {
            // Open the modal and pass the data to it
            this.openModal(this.modal3, data);
          })
          .catch((error: any) => {
            console.error(error);
          });
      }
      if (node.data.text === 'R29') {
        // Call the API and get the data
        this.getDataFromAPISWSFAX()
          .then((data: any) => {
            // Open the modal and pass the data to it
            this.openModal(this.modal4, data);
          })
          .catch((error: any) => {
            console.error(error);
          });
      }
    });


  }
  // Add the tableData property to your TopologyComponent
  tableData: any[] = [];

  getDataFromAPISWSFAX() {
    return fetch('http://localhost:8077/api/v1/Device/getInfoDeviceById?DeviceName=SW-EDGE-SFAX')
      .then(response => response.json())
      .then(data => {
        const cpuValue = data.results.cpuValue;
        const memoryValue = data.results.memoryValue;
        // Update the RAM data
        this.ramData = [
          { name: 'RAM Total en Mo', value: (943607216  - memoryValue)   / (1024 * 1024) },
          { name: 'RAM Utiliséen Mo', value: memoryValue  / (1024 * 1024) }
        ];

        // Update the CPU data
        this.cpuData = [
          { name: 'CPU libre  ', value: 4 },
          { name: 'CPU Utilisé ', value: cpuValue }
          
        ];
        const netInterfaces = data.results.netInterfaces;
        const tableData = netInterfaces.map((netInterface: any) => {
          const interfaceState = netInterface.interfaceState === "true" ? "Up" : "Down";
          const interfaceColor = netInterface.interfaceState === "true" ? "green" : "red";
          return {
            nameInterface: netInterface.nameInterface,
            interfaceState: interfaceState,
            interfaceColor: interfaceColor
          };
        });
        // Assign the table data to the tableData property
        this.tableData = tableData;
          
    
      })
      .catch(error => console.error(error));
  }

  getDataFromAPISWTUNIS() {
    return fetch('http://localhost:8077/api/v1/Device/getInfoDeviceById?DeviceName=SW-EDGE-TUNIS')
      .then(response => response.json())
      .then(data => {
        const cpuValue = data.results.cpuValue;
        const memoryValue = data.results.memoryValue;
      
        this.ramData = [
          { name: 'RAM Total en Mo', value: (943607216  - memoryValue)   / (1024 * 1024) },
          { name: 'RAM Utiliséen Mo', value: memoryValue  / (1024 * 1024) }
        ];
        this.cpuData = [
          { name: 'CPU libre  ', value: 4 },
          { name: 'CPU Utilisé ', value: cpuValue }
          
        ];
        // Extract the table data
        const netInterfaces = data.results.netInterfaces;
        const tableData = netInterfaces.map((netInterface: any) => {
          const interfaceState = netInterface.interfaceState === "true" ? "Up" : "Down";
          const interfaceColor = netInterface.interfaceState === "true" ? "green" : "red";
          return {
            nameInterface: netInterface.nameInterface,
            interfaceState: interfaceState,
            interfaceColor: interfaceColor
          };
        });

        // Assign the table data to the tableData property
        this.tableData = tableData;
      })
      .catch(error => console.error(error));
  }

  getDataFromAPICETUNIS() {
    return fetch('http://localhost:8077/api/v1/Device/getInfoDeviceById?DeviceName=CE-TUNIS')
      .then(response => response.json())
      .then(data => {
        const cpuValue = data.results.cpuValue;
        const memoryValue = data.results.memoryValue;
        const remainingValue = 100 - cpuValue;
        this.ramData = [
          { name: 'RAM Total en Mo', value: (968449952  - memoryValue)   / (1024 * 1024) },
          { name: 'RAM Utiliséen Mo', value: memoryValue  / (1024 * 1024) }
        ];

        this.cpuData = [
          { name: 'CPU libre  ', value: 4 },
          { name: 'CPU Utilisé ', value: cpuValue }
          
        ];
        // Extract the table data
        const netInterfaces = data.results.netInterfaces;
        const tableData = netInterfaces.map((netInterface: any) => {
          const interfaceState = netInterface.interfaceState === "true" ? "Up" : "Down";
          const interfaceColor = netInterface.interfaceState === "true" ? "green" : "red";
          return {
            nameInterface: netInterface.nameInterface,
            interfaceState: interfaceState,
            interfaceColor: interfaceColor
          };
        });

        // Assign the table data to the tableData property
        this.tableData = tableData;
      })
      .catch(error => console.error(error));
  }
 
  getDataFromAPICESFAX() {
    return fetch('http://localhost:8077/api/v1/Device/getInfoDeviceById?DeviceName=CE-SFAX')
      .then(response => response.json())
      .then(data => {
        const cpuValue = data.results.cpuValue;
        const memoryValue = data.results.memoryValue;
        this.ramData = [
          { name: 'RAM libre en Mo', value: (968449952 - memoryValue)   / (1024 * 1024),},
          { name: 'RAM Utilisé en Mo', value: memoryValue  / (1024 * 1024) }
        ];

        this.cpuData = [
          { name: 'CPU libre  ', value: 4 },
          { name: 'CPU Utilisé ', value: cpuValue }
          
        ];
     
        // Extract the table data
        const netInterfaces = data.results.netInterfaces;
        const tableData = netInterfaces.map((netInterface: any) => {
          const interfaceState = netInterface.interfaceState === "true" ? "Up" : "Down";
          const interfaceColor = netInterface.interfaceState === "true" ? "green" : "red";
          return {
            nameInterface: netInterface.nameInterface,
            interfaceState: interfaceState,
            interfaceColor: interfaceColor
          };
        });

        // Assign the table data to the tableData property
        this.tableData = tableData;

      })
      .catch(error => console.error(error));
      
  }


  openModal(modal: TemplateRef<any>, data: any) {
    // this.modalData = data.tableData; // Assuming the data contains the table data for the modal
    this.modalRef = this.modalService.open(modal, { size: 'lg' });
    this.modalRef.componentInstance.data = data;
  }

  // Add this method to close the modal
  closeModal() {
    if (this.modalRef) {
      this.modalRef.close();
    }
  }



}



