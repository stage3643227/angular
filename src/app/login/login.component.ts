import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr'
import { User } from '../model/user';
import { AuthService } from '../service/auth.service';
import { APIResponse } from '../model/apiResponseUser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  userName: any;
  password:any;
  constructor(private builder: FormBuilder, private toastr: ToastrService, private service: AuthService,
    private router: Router) {
      sessionStorage.clear();
  }
  result: any;
    loginform = this.builder.group({
    email: this.builder.control('', Validators.required),
    password: this.builder.control('', Validators.required)
  });

  proceedlogin() {
    if (this.loginform.valid) {
      this.service.postUser(this.userName,this.password).subscribe((apiResponse: APIResponse<User[]>) => {
        this.result = apiResponse;
        if (apiResponse.status === 'Success')  {
           sessionStorage.setItem('id',this.result.results.idUser);
            sessionStorage.setItem('email',this.result.results.email);
            sessionStorage.setItem('role',this.result.results.role.roleName);
            sessionStorage.setItem('idRole',this.result.results.role.idRole);
            sessionStorage.setItem('idCompany', this.result.results.idCompany);
            this.router.navigate(['/userspace']);
        } else {
          this.toastr.error('Invalid credentials');
        }
      });
    }else {
      this.toastr.warning("S'il vous plaît veuillez entrer des données valides.");
    }}}