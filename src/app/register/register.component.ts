import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { Role } from '../model/role';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  sideNavStatus: boolean = false;
  registerForm!: FormGroup;
  rolelist: Role[] = [];

  constructor(
    private builder: FormBuilder,
    private service: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.service.getuserpermission().subscribe((res) => {
      this.rolelist = res.idRole;
    });

    this.service.getuserrole().subscribe((res) => {
      console.log(res);
      this.rolelist = res.results.filter((item) => item?.roleName !== 'Admin');
    });

    this.registerForm = this.builder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z\\d]{8,}')]
      ],
      idCompany: [sessionStorage.getItem('idCompany')],
      idRole: ['', Validators.required]
    });
  }

  getErrorMessage(controlName: string): string {
    const control = this.registerForm.get(controlName);
    if (control?.hasError('required')) {
      return 'Ce champ est obligatoire';
    } else if (control?.hasError('email')) {
      return `Veuillez saisir une adresse email valide pour le champ ${controlName}`;
    } else if (control?.hasError('pattern')) {
      return 'Ce champ doit contenir minimum: 8 caractères, une lettre minuscule, une lettre majuscule et un chiffre.';
    }
    return '';
  }

  proceedregister() {
    this.service.createNewUser(this.registerForm.value).subscribe(
      (result) => {
        this.toastr.success('Félicitations ! Votre utilisateur est ajouté avec succès !');
        this.registerForm.reset();
        this.router.navigate(['/user']);
      },
      (error) => {
        console.error(error);
        if (error.error && error.error.errors) {
          console.log(error.error.errors); // Afficher les erreurs renvoyées par le serveur
        }
        this.toastr.error("Une erreur s'est produite lors de la création de l'utilisateur. Veuillez réessayer.");
      }
    );
  }
}
