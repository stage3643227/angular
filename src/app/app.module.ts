import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule  } from 'ngx-toastr';
import { UpdatepopupComponent } from './updatepopup/updatepopup.component';
import { CustomerComponent } from './customer/customer.component';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { MapComponent } from './map/map.component';
import { TopologyComponent } from './topology/topology.component';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { GojsAngularModule } from 'gojs-angular';
import { TelecomComponent } from './telecom/telecom.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { AccueilComponent } from './accueil/accueil.component';
import { UserspaceComponent } from './userspace/userspace.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    UserComponent,
    UpdatepopupComponent,
    CustomerComponent,
    HeaderComponent,
    SideNavComponent,
    MapComponent,
    TopologyComponent,
    TelecomComponent,
    AccueilComponent,
    UserspaceComponent,
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxChartsModule,
    HttpClientModule,
    MatListModule,
    MatFormFieldModule,
    GojsAngularModule,
    MatDialogModule,
    FormsModule,
    BrowserModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
      timeOut: 1500,
      
    } )
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    MatDialogModule
  ]
})
export class AppModule { }
